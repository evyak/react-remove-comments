import './App.css';
import { React, useState } from 'react'
import { BrowserRouter, Routes, Route, Navigate, useNavigate } from "react-router-dom";
import Main from "./components/Main"

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
        </header>
        <h1>
          File Reader Application
        </h1>
        <Main></Main>
      </div>
    </BrowserRouter>
  );
}

export default App;
