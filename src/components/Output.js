import React from 'react';
import { Link } from "react-router-dom";
import { FileTextContext } from '../file-context';
import './Output.css';

function Output() {

    return (
        <>
            <div className="output">
                <FileTextContext.Consumer>
                    {({ fileText }) => (
                        <>
                            <h2>
                                Text from the file without comments:
                            </h2>
                            <p className="output-result">
                                {fileText}
                            </p>
                        </>
                    )}
                </FileTextContext.Consumer>
            </div>
            <Link className="readfile-link" to="/read-file">Read Another File</Link>
        </>
    )
}
export default Output;
