/* eslint-disable no-useless-escape */
import './Main.css';
import { React, useState } from 'react'
import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
import ReadFile from "./ReadFile"
import Output from "./Output"

import { FileTextContext } from '../file-context';


function Main() {
    let navigate = useNavigate();

    const [fileText, setFileText] = useState(null);

    //using the FileReader api to read the text from the txt file as a string
    const changeFileText = (textFile) => {
        console.log('file uploaded')
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            let fileText = reader.result;
            cleanFile(fileText)
            navigate(`output`)
        }, false);
        reader.readAsText(textFile);
    }

    // clean the file from single line comments - "//"
    //and multiline comments - /* */
    //could have used more of a stack approach
    //but since it is fairly simple used simple "statuses" for current step
    //to know when the file text string is in/entering/exiting/not in a comment
    const cleanFile = (fileText) => {
        const strArr = fileText.split("");
        let cleanedText = '';
        let multilineComment = false;
        let enterSingleLineCommentOnSlash = false;
        let enterMultiLineOnStar = false;
        let exitMultilineOnSlash = false;
        let skipUntilNewLine = false;

        strArr.forEach((char) => {
            //if inside multline comment
            if (multilineComment){
                if (char === '*'){
                    exitMultilineOnSlash = true;
                }
                if ( exitMultilineOnSlash && char === '\/'){
                    multilineComment = false;
                    exitMultilineOnSlash = false;
                }

                if (exitMultilineOnSlash && char !== '*'){
                    exitMultilineOnSlash = false;
                }

                return;
            }

            // if inside singleline comment
            if (skipUntilNewLine){
                if (char === '\n'){
                    skipUntilNewLine = false;
                }
                return;
            }

            // need only a "*" to enter a multiline comment
            if (enterMultiLineOnStar && char === '*'){
                enterSingleLineCommentOnSlash = false;
                enterMultiLineOnStar= false;
                multilineComment = true;
                return;
            }

            // need only a "\" to enter a single line comment
            if (enterSingleLineCommentOnSlash && char === '/'){
                enterMultiLineOnStar = false;
                enterSingleLineCommentOnSlash = false;
                skipUntilNewLine = true;
                return;
            }

            // if its a '/* - could next enter either kind of comment
            if (char === '/'){
                enterSingleLineCommentOnSlash = true;
                enterMultiLineOnStar = true;
                return;
            }

            // if it's not a "/" and last was "/" - since it was not a comment add the last "/" to the text
            if (char !== '/' && enterSingleLineCommentOnSlash && enterMultiLineOnStar){
                cleanedText += '/';
            }

            cleanedText += char;
        });
        setFileText(cleanedText)
    }

    return (
        <main className="top-container">
            <FileTextContext.Provider value={{ fileText, changeFileText }}>
                <Routes>
                    <Route path="/" element={<Navigate to="/read-file" />} />
                    <Route path="read-file" element={<ReadFile />} />
                    <Route path="output" element={<Output />} />
                </Routes>
            </FileTextContext.Provider>
        </main>
    );
}

export default Main;
