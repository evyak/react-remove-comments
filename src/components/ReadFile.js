import React, { useState, useEffect, useRef, useContext } from 'react';
import './ReadFile.css';

import { FileTextContext } from '../file-context';

export default function ReadFile() {
  const { fileText, changeFileText } = useContext(FileTextContext);
  const [selectedFile, setSelectedFile] = useState(null);
  const didMountRef = useRef(false);

  useEffect(() => {
    //prevent activation on first render
    if (!didMountRef.current) {
      didMountRef.current = true;
      return;
    } else {
      console.log('file uploaded')
    }
  }, [selectedFile])

  const fileChange = (e) => {
    e.preventDefault();
    setSelectedFile(e.target.files[0]);
  }

  const submitForm = (e) => {
    e.preventDefault();
    changeFileText(selectedFile);
  }

  return (
    <>
      <section className="upload-file-container">
        <form>
          <input
            type="file"
            onChange={fileChange}
          />
          <button onClick={submitForm}>Submit</button>
        </form>
      </section>
    </>
  )
}
